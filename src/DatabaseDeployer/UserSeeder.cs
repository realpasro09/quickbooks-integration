using System;
using QuickbooksIntegration.Domain.Entities;
using QuickbooksIntegration.Domain.Services;
using DomainDrivenDatabaseDeployer;
using NHibernate;

namespace DatabaseDeployer
{
    public class UserSeeder : IDataSeeder
    {
        readonly ISession _session;

        public UserSeeder(ISession session)
        {
            _session = session;
        }

        #region IDataSeeder Members

        public void Seed()
        {
            var encryptor = new HashPasswordEncryptor();

            var admiRole = new Role(Guid.NewGuid(), "Administrator");
            var basicRole = new Role(Guid.NewGuid(), "Basic");
            var userEmailLogin = new UserEmailLogin("Test User", "test@test.com", encryptor.Encrypt("password"), "615-555-1212");
            var userEmailLogin2 = new UserEmailLogin("Test User 2", "test2@test.com", encryptor.Encrypt("password"), "615-555-1212");
            var userEmailLogin3 = new UserEmailLogin("Test User 3", "test3@test.com", encryptor.Encrypt("password"), "615-555-1212");
            var userEmailLogin4 = new UserEmailLogin("Test User 4", "quickbooks@test.com", encryptor.Encrypt("password"), "615-555-1212");
            var userEmailLogin5 = new UserEmailLogin("Test User 4", "quickbooks2@test.com", encryptor.Encrypt("password"), "615-555-1212");
            var userEmailLogin6 = new UserEmailLogin("Test User 4", "quickbooks3@test.com", encryptor.Encrypt("password"), "615-555-1212");
            var userEmailLogin7 = new UserEmailLogin("Global", "global2014@quickbooks.com", encryptor.Encrypt("Chanel@2014"), "615-555-1212");

            var client1 = new UserEmailLogin("Client 1", "client1@test.com", encryptor.Encrypt("password"), "615-555-1212");
            var client2 = new UserEmailLogin("client 2", "client2@test.com", encryptor.Encrypt("password"), "615-555-1212");
            var client3 = new UserEmailLogin("client 3", "client3@test.com", encryptor.Encrypt("password"), "615-555-1212");

            userEmailLogin.AddRol(basicRole);
            userEmailLogin2.AddRol(basicRole);
            userEmailLogin3.AddRol(basicRole);
            userEmailLogin4.AddRol(basicRole);
            userEmailLogin5.AddRol(basicRole);
            userEmailLogin6.AddRol(basicRole);
            userEmailLogin7.AddRol(basicRole);

            client1.AddRol(basicRole);
            client2.AddRol(basicRole);
            client3.AddRol(basicRole);
            var administratorUser = new UserEmailLogin("Admin User", "admin@test.com", encryptor.Encrypt("password"),
                "123");
            administratorUser.AddRol(admiRole);
            administratorUser.AddRol(basicRole);

            var userAbility = new UserAbility("Developer");
            _session.Save(userAbility);
            userEmailLogin.AddAbility(userAbility);
            userEmailLogin2.AddAbility(userAbility);
            userEmailLogin3.AddAbility(userAbility);
            userEmailLogin4.AddAbility(userAbility);
            userEmailLogin5.AddAbility(userAbility);
            userEmailLogin6.AddAbility(userAbility);
            userEmailLogin7.AddAbility(userAbility);

            client1.AddAbility(userAbility);
            client2.AddAbility(userAbility);
            client3.AddAbility(userAbility);

            _session.Save(admiRole);
            _session.Save(basicRole);


            _session.Save(userEmailLogin);
            _session.Save(userEmailLogin2);
            _session.Save(userEmailLogin3);
            _session.Save(userEmailLogin4);
            _session.Save(userEmailLogin5);
            _session.Save(userEmailLogin6);
            _session.Save(userEmailLogin7);
            _session.Save(client1);
            _session.Save(client2);
            _session.Save(client3);
            _session.Save(administratorUser);


        }

        #endregion
    }
}