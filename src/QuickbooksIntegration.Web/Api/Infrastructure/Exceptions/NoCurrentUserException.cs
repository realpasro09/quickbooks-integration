using System;

namespace QuickbooksIntegration.Web.Api.Infrastructure.Exceptions
{
    public class NoCurrentUserException : Exception
    {
    }
}