using System;

namespace QuickbooksIntegration.Web.Api.Infrastructure.Authentication
{
    public class TokenDoesNotExistException : Exception
    {
    }
}