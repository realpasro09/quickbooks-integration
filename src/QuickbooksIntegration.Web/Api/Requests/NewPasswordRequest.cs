namespace QuickbooksIntegration.Web.Api.Requests
{
    public class NewPasswordRequest
    {
        public string Password { get; set; }
    }
}