using System;

namespace QuickbooksIntegration.Web.Api.Requests
{
    public class UserAbilityRequest
    {
        public string Description { get; set; }
        public Guid Id { get; set; }
    }
}