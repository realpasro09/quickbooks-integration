﻿namespace QuickbooksIntegration.Web.Api.Requests.Admin
{
    public class OauthTokenRequest
    {
        public string OauthToken { get; set; }
        public string OauthVerifier { get; set; }
        public string RealmId { get; set; }
        public string DataSource { get; set; }
        public string UserEmail { get; set; }
    }
}