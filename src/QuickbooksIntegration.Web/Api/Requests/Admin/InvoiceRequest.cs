﻿using System;
using Intuit.Ipp.Data;

namespace QuickbooksIntegration.Web.Api.Requests.Admin
{
    public class InvoiceRequest
    {
        public string SOSInvoiceNumber { get; set; }
        public string LoadNumber { get; set; }
        public string UserEmail { get; set; }
        public string BrokerName { get; set; }
        public string BillingAddress { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string DueDate { get; set; }
        public string InvoiceDate { get; set; }
        public decimal BasicFreightRate { get; set; }
        public string CustomerReferenceNumber { get; set; }
        public string PickUpDate { get; set; }
        public string PickUpCity { get; set; }
        public string DeliveryCity { get; set; }
        public decimal ExtraPickUp { get; set; }
        public decimal ExtraDelivery { get; set; }
        public decimal WaitingTime { get; set; }
        public decimal LumperFees { get; set; }
        public decimal Overnight { get; set; }
        public decimal Others { get; set; }
        public decimal FuelSurcharge { get; set; }
        public decimal Discount { get; set; }
        public decimal BrokerAdvanceFee { get; set; }
        public decimal BrokerAdvance { get; set; }
        public decimal BondFee { get; set; }
        public decimal CongestionFee { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal RestakingFee { get; set; }
        public decimal PortEntry { get; set; }
    }
}