﻿using System.Collections.Generic;

namespace QuickbooksIntegration.Web.Api.Requests.Admin
{
    public class InvoicesRequest
    {
        public InvoiceRequest[] Invoices;
        public string UserEmail { get; set; }
    }
}