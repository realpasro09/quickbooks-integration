﻿namespace QuickbooksIntegration.Web.Api.Requests.Admin
{
    public class DirectoryRequest
    {
        public string UserEmail { get; set; }
        public string Directory { get; set; }
    }
}