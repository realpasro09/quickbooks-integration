using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using AcklenAvenue.Commands;
using QuickbooksIntegration.Domain.Application.Commands;
using QuickbooksIntegration.Domain.Entities;
using QuickbooksIntegration.Domain.Services;
using AutoMapper;
using DevDefined.OAuth.Consumer;
using DevDefined.OAuth.Framework;
using Nancy;
using Nancy.ModelBinding;
using QuickbooksIntegration.Web.Api.Infrastructure;
using QuickbooksIntegration.Web.Api.Requests;
using QuickbooksIntegration.Web.Api.Requests.Facebook;
using QuickbooksIntegration.Web.Api.Requests.Google;

namespace QuickbooksIntegration.Web.Api.Modules
{

    public class UserAccountModule : NancyModule
    {
        public static String RequestTokenUrl = ConfigurationManager.AppSettings["GET_REQUEST_TOKEN"];
        public static String AccessTokenUrl = ConfigurationManager.AppSettings["GET_ACCESS_TOKEN"];
        public static String AuthorizeUrl = ConfigurationManager.AppSettings["AuthorizeUrl"];
        public static String OauthUrl = ConfigurationManager.AppSettings["OauthLink"];
        public String ConsumerKey = ConfigurationManager.AppSettings["ConsumerKey"];
        public String ConsumerSecret = ConfigurationManager.AppSettings["ConsumerSecret"];
        public string StrrequestToken = string.Empty;
        public string TokenSecret = string.Empty;
        public string OauthCallbackUrl = "http://localhost:8001/#/quickbooksConnect";

        public UserAccountModule(IReadOnlyRepository readOnlyRepository,ICommandDispatcher commandDispatcher, IPasswordEncryptor passwordEncryptor, IMappingEngine mappingEngine,
            IWriteableRepository writeableRepository)
        {
            Post["/register"] =
                _ =>
                    {
                        var req = this.Bind<NewUserRequest>();
                        var abilities = mappingEngine.Map<IEnumerable<UserAbilityRequest>, IEnumerable<UserAbility>>(req.Abilities);
                        commandDispatcher.Dispatch(this.UserSession(),
                                                   new CreateEmailLoginUser(req.Email, passwordEncryptor.Encrypt(req.Password), req.Name, req.PhoneNumber, abilities));
                        return null;
                    };

            Get["/qbInit/{userEmail}/{host}"] = _ =>
            {
                var userEmail = (string)_.userEmail;
                var host = (string)_.host;
                var session = CreateSession();
                var requestToken = session.GetRequestToken();
                TokenSecret = requestToken.Token;
                var authLink = CreateAuthorization(TokenSecret, host);
                var token = requestToken;
                StrrequestToken = token.Token;
                var user = readOnlyRepository.Query<User>(x => x.Email == userEmail).First();
                user.RequestToken = requestToken.Token;
                user.RequestTokenSecret = requestToken.TokenSecret;
                user.RequestSessionHandle = requestToken.SessionHandle;
                writeableRepository.Update(user);
                return authLink;
            };


            Post["/password/requestReset"] =
                _ =>
                {
                    var req = this.Bind<ResetPasswordRequest>();
                    commandDispatcher.Dispatch(this.UserSession(),
                                               new CreatePasswordResetToken(req.Email) );
                    return null;
                };

            Put["/password/reset/{token}"] =
                p =>
                {
                    var newPasswordRequest = this.Bind<NewPasswordRequest>();
                    var token = Guid.Parse((string)p.token);
                    commandDispatcher.Dispatch(this.UserSession(),
                                               new ResetPassword(token, passwordEncryptor.Encrypt(newPasswordRequest.Password)));
                    return null;
                };

            Post["/user/abilites"] = p =>
            {

                var requestAbilites = this.Bind<UserAbilitiesRequest>();
                commandDispatcher.Dispatch(this.UserSession(), new AddAbilitiesToUser(requestAbilites.UserId, requestAbilites.Abilities.Select(x => x.Id)));

                return null;


            };

            Get["/abilities"] = _ =>
            {
                var abilites = readOnlyRepository.GetAll<UserAbility>();

                var mappedAbilites = mappingEngine.Map<IEnumerable<UserAbility>, IEnumerable<UserAbilityRequest>>(abilites);

                return mappedAbilites;
            };
        }
        protected string CreateAuthorization(string tokenSecret, string host)
        {
            var url = "http://";
            if (host.Contains("localhost"))
            {
                url += host + ":8001/#/quickbooksConnect";
            }
            else
            {
                url += host + "/#/quickbooksConnect";
            }
            var authUrl = string.Format("{0}?oauth_token={1}&oauth_callback={2}", AuthorizeUrl, tokenSecret, UriUtility.UrlEncode(url));
            var newOauthLink = authUrl;
            return newOauthLink;
        }
        protected IOAuthSession CreateSession()
        {
            var consumerContext = new OAuthConsumerContext
            {
                ConsumerKey = ConsumerKey,
                ConsumerSecret = ConsumerSecret,
                SignatureMethod = SignatureMethod.HmacSha1
            };
            return new OAuthSession(consumerContext,
                                    RequestTokenUrl,
                                    OauthUrl,
                                    AccessTokenUrl);
        }
    }
}