﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Excel;
using Nancy;
using Nancy.ModelBinding;
using QuickbooksIntegration.Domain.Entities;
using QuickbooksIntegration.Domain.Services;
using QuickbooksIntegration.Web.Api.Requests.Admin;
using System.Configuration;

namespace QuickbooksIntegration.Web.Api.Modules
{
    public class InvoiceModule : NancyModule
    {
        public InvoiceModule( IReadOnlyRepository readOnlyRepository,
            IWriteableRepository writeableRepository)
        {
            Post["/saveDirectory"] =
                _ =>
                {
                    var request = this.Bind<DirectoryRequest>();
                    var user = readOnlyRepository.Query<User>(x => x.Email == request.UserEmail).First();
                    user.FtpFolder = request.Directory;
                    writeableRepository.Update(user);
                    return "OK";
                };

            Get["/checkUserFolder/{userEmail}"] =
                _ =>
                {
                    var userEmail = (string)_.userEmail;
                    var user = readOnlyRepository.Query<User>(x => x.Email == userEmail).First();

                    return !string.IsNullOrEmpty(user.FtpFolder);
                };
            
            Get["/syncedInvoices/{userEmail}"] =
                _ =>
                {
                    var userEmail = (string)_.userEmail;
                    var user = readOnlyRepository.Query<User>(x => x.Email == userEmail).First();

                    var invoices =
                        readOnlyRepository.Query<Invoice>(x => x.User == user).OrderByDescending(x=>x.SyncDate).Take(30).ToList();

                    return invoices;
                };
            Get["/syncedDriverSettlement/{userEmail}"] =
                _ =>
                {
                    var userEmail = (string)_.userEmail;
                    var user = readOnlyRepository.Query<User>(x => x.Email == userEmail).First();

                    var driverSettlements =
                        readOnlyRepository.Query<DriverSettlement>(x => x.User == user).OrderByDescending(x => x.SyncDate).Take(30).ToList();

                    return driverSettlements;
                };
            Get["/pendingInvoices/{userEmail}"] =
                _ =>
                {
                    var userEmail = (string)_.userEmail;
                    var user = readOnlyRepository.Query<User>(x => x.Email == userEmail).First();

                    var invoicesToSync = readOnlyRepository.Query<Invoice>(x => 
                    x.User == user && 
                    x.QbId == null && 
                    x.Status == null
                    && !x.InSyncProcess).ToList();

                    return invoicesToSync;
                };
            Get["/pendingDriverSettlement/{userEmail}"] =
                _ =>
                {
                    var userEmail = (string)_.userEmail;
                    var user = readOnlyRepository.Query<User>(x => x.Email == userEmail).First();

                    var invoicesToSync = readOnlyRepository.Query<DriverSettlement>(x => x.User == user && x.QbId == null && x.Status == null).ToList();

                    return invoicesToSync;
                };

            Get["/checkFtpInvoices/{userEmail}"] =
                _ =>
                {
                    var userEmail = (string)_.userEmail;
                    var user = readOnlyRepository.Query<User>(x => x.Email == userEmail).First();

                    GetInvoiceDataFromFtp(readOnlyRepository, writeableRepository, user);
                    GetDriverSettlementDataFromFtp(readOnlyRepository, writeableRepository, user);

                    return "OK";
                };
            Post["/insertInvoices"] =
                _ =>
                {
                    var file = Request.Files.First();
                    string email = Request.Form["userEmail"].ToString();
                    var user = readOnlyRepository.Query<User>(x => x.Email == email).First();
                    if (file.ContentType != "application/vnd.ms-excel")
                    {
                        var excelReader = ExcelReaderFactory.CreateOpenXmlReader(file.Value);
                        var result = excelReader.AsDataSet();
                        var count = 1;
                        foreach (DataRow row in result.Tables[0].Rows)
                        {
                            var row1 = row;
                            if ((string) row1[0] != "DatabaseName")
                            {
                                
                                var invoiceCheck =
                                    readOnlyRepository.Query<Invoice>(x => x.SosInvoiceNumber == row1[7].ToString())
                                        .FirstOrDefault();
                                if (invoiceCheck == null)
                                {
                                    InsertNewInvoice(writeableRepository, user, row);
                                }
                                else
                                {
                                    EditImportedInvoice(writeableRepository, invoiceCheck, row);
                                }

                            }

                            count++;
                        }
                    }
                    else
                    {
                        ImportCsvFile(readOnlyRepository, writeableRepository, file.Value, user);
                    }

                    return "OK";
                };
            
        }

        private static void GetDriverSettlementDataFromFtp(IReadOnlyRepository readOnlyRepository,
            IWriteableRepository writeableRepository,
            User user)
        {
            try
            {
                var baseUserPath = GetFtpUrl(); // + user.FtpFolder;
                string ftpRequestUsername = GetFtpUsername();
                string ftpRequestPassword = GetFtpPassword();
                const string pendingInvoicesPath = "/PendingSettlements/"; // "/driversettlements/pending/";
                var request = (FtpWebRequest)WebRequest.Create(baseUserPath + pendingInvoicesPath);
                request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;

                request.Credentials = new NetworkCredential(ftpRequestUsername, ftpRequestPassword);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                var response = (FtpWebResponse)request.GetResponse();
                var reader = new StreamReader(response.GetResponseStream());
                var list = reader.ReadToEnd().Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var line in list)
                {
                    var lastSpace = line.LastIndexOf(' ') + 1;
                    var len = line.Length;
                    var namelen = len - lastSpace;
                    var subs = line.Substring(lastSpace, namelen);
                    var name = subs;
                    var filePath = baseUserPath + pendingInvoicesPath + name;

                    using (var requests = new WebClient())
                    {
                        requests.Credentials = new NetworkCredential(ftpRequestUsername, ftpRequestPassword);
                        var fileData = requests.DownloadData(filePath);
                        var streamFile = new MemoryStream(fileData);
                        ImportDriverSettlementsFile(readOnlyRepository, writeableRepository, streamFile, user);
                    }
                    MoveFile(baseUserPath, ftpRequestUsername, ftpRequestPassword, "/PendingSettlements/",
                        // user.FtpFolder + "/driversettlements/pending/",
                        "/ProcessedSettlements/", name); 
                        //user.FtpFolder + "/driversettlements/synced/", name);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Not Driver Settlement Folder Found");
            }
        }
        private static void GetInvoiceDataFromFtp(IReadOnlyRepository readOnlyRepository, IWriteableRepository writeableRepository,
            User user)
        {
            
            try
            {
                var baseUserPath = GetFtpUrl(); // + user.FtpFolder;
                string ftpRequestUsername = GetFtpUsername();
                string ftpRequestPassword = GetFtpPassword();
                const string pendingInvoicesPath = "/PendingBilling/";
                var request = (FtpWebRequest)WebRequest.Create(baseUserPath + pendingInvoicesPath);
                request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;

                request.Credentials = new NetworkCredential(ftpRequestUsername, ftpRequestPassword);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                var response = (FtpWebResponse)request.GetResponse();
                var reader = new StreamReader(response.GetResponseStream());
                var list = reader.ReadToEnd().Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var line in list)
                {
                    var lastSpace = line.LastIndexOf(' ') + 1;
                    var len = line.Length;
                    var namelen = len - lastSpace;
                    var subs = line.Substring(lastSpace, namelen);
                    var name = subs;
                    var filePath = baseUserPath + pendingInvoicesPath + name;

                    using (var requests = new WebClient())
                    {
                        requests.Credentials = new NetworkCredential(ftpRequestUsername, ftpRequestPassword);    
                        var fileData = requests.DownloadData(filePath);
                        var streamFile = new MemoryStream(fileData);
                        ImportCsvFile(readOnlyRepository, writeableRepository, streamFile, user);
                    }
                    MoveFile(baseUserPath, ftpRequestUsername, ftpRequestPassword, "/PendingBilling/",
                        "/ProcessedBilling/", name);

                }
            }
            catch (Exception e)
            {
                throw new Exception("User Folder not found");
            }
            
        }
        public static string GetRelativePath(string ftpBasePath, string ftpToPath)
        {

            if (!ftpBasePath.StartsWith("/"))
            {
                throw new Exception("Base path is not absolute");
            }
            ftpBasePath = ftpBasePath.Substring(1);
            if (ftpBasePath.EndsWith("/"))
            {
                ftpBasePath = ftpBasePath.Substring(0, ftpBasePath.Length - 1);
            }

            if (!ftpToPath.StartsWith("/"))
            {
                throw new Exception("Base path is not absolute");
            }
            ftpToPath = ftpToPath.Substring(1);
            if (ftpToPath.EndsWith("/"))
            {
                ftpToPath = ftpToPath.Substring(0, ftpToPath.Length - 1);
            }
            var arrBasePath = ftpBasePath.Split("/".ToCharArray());
            var arrToPath = ftpToPath.Split("/".ToCharArray());

            var basePathCount = arrBasePath.Count();
            var levelChanged = basePathCount;
            for (var iIndex = 0; iIndex < basePathCount; iIndex++)
            {
                if (arrToPath.Count() <= iIndex) continue;
                if (arrBasePath[iIndex] == arrToPath[iIndex]) continue;
                levelChanged = iIndex;
                break;
            }
            var howManyBack = basePathCount - levelChanged;
            var sb = new StringBuilder();
            for (var i = 0; i < howManyBack; i++)
            {
                sb.Append("../");
            }
            for (var i = levelChanged; i < arrToPath.Count(); i++)
            {
                sb.Append(arrToPath[i]);
                sb.Append("/");
            }

            return sb.ToString();
        }

        public static string MoveFile(string ftpuri, string username, string password, string ftpfrompath, string ftptopath, string filename)
        {
            var serverFile = new Uri(ftpuri + ftpfrompath + filename);
            var reqFtp = (FtpWebRequest)FtpWebRequest.Create(serverFile);
            reqFtp.Method = WebRequestMethods.Ftp.Rename;
            reqFtp.Credentials = new NetworkCredential(username, password);
            var date = DateTime.Now;
            var splitName = filename.Split('.');
            var newFileName = splitName[0] + "_" + date.Year + date.Month + date.Month + date.Second + date.Millisecond + "." + splitName[1];
            reqFtp.RenameTo = GetRelativePath(ftpfrompath, ftptopath) + newFileName;

            var ftpresponse = (FtpWebResponse)reqFtp.GetResponse();
            var responseStream = ftpresponse.GetResponseStream();
            var reader = new StreamReader(responseStream);
            return reader.ReadToEnd();
        }

        private static void ImportDriverSettlementsFile(IReadOnlyRepository readOnlyRepository,
            IWriteableRepository writeableRepository,
            Stream file, User user)
        {
            var reader = new StreamReader(file);

            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (line == null) continue;
                var row = line.Split(';');
                if (row[0].Contains("Truck Owner") || row[0].Contains("TruckOwner")) continue;
                var driverSettlement =
                    readOnlyRepository.Query<DriverSettlement>(x => x.FileNumber == row[3].ToString())
                        .FirstOrDefault();
                if (driverSettlement != null)
                {
                    driverSettlement.MarkAsUpdate = false;
                    writeableRepository.Update(driverSettlement);
                    continue;
                }
                var driverItem = new DriverSettlement()
                {
                    User = user,
                    TruckOwner = row[0].ToString().Trim(),
                    Driver = row[1].ToString().Trim(),
                    TruckNumber = row[2].ToString().Trim(),
                    FileNumber = row[3].ToString().Trim(),
                    OoPay = row[4].ToString().Trim(),
                    BasicDriverPay = row[5].ToString().Trim(),
                    WaitingTime = row[6].ToString().Trim(),
                    ExtraPickUp = row[7].ToString().Trim(),
                    ExtraDelivery = row[8].ToString().Trim(),
                    Overnight = row[9].ToString().Trim(),
                    Others = row[10].ToString().Trim(),
                    EmptyMileDebit = row[11].ToString().Trim(),
                    ExtraMiles = row[12].ToString().Trim(),
                    FuelSurcharge = row[13].ToString().Trim(),
                    SubTotal1 = row[14].ToString().Trim(),
                    Fuel = row[15].ToString().Trim(),
                    DriverCash = row[16].ToString().Trim(),
                    FuelFee = row[17].ToString().Trim(),
                    RefFuel = row[18].ToString().Trim(),
                    FuelRebate = row[19].ToString().Trim(),
                    SubTotal2 = row[20].ToString().Trim(),
                    EfsCashAdvances = row[21].ToString().Trim(),
                    EfsFee = row[22].ToString().Trim(),
                    WesternUnion = row[23].ToString().Trim(),
                    OwnChecks = row[24].ToString().Trim(),
                    Transcheck = row[25].ToString().Trim(),
                    AdvanceFee = row[26].ToString().Trim(),
                    Loan = row[27].ToString().Trim(),
                    LoanFee = row[28].ToString().Trim(),
                    NegativeFile = row[29].ToString().Trim(),
                    QuickPayFee = row[30].ToString().Trim(),
                    QuickPayPercent = row[31].ToString().Trim(),
                    SubTotal3 = row[32].ToString().Trim(),
                    TruckLease = row[33].ToString().Trim(),
                    LicensePlate = row[34].ToString().Trim(),
                    RoadTax = row[35].ToString().Trim(),
                    TrailerLease = row[36].ToString().Trim(),
                    TrailerRent = row[37].ToString().Trim(),
                    SubTotal4 = row[38].ToString().Trim(),
                    LumperFees = row[39].ToString().Trim(),
                    ReDiesel = row[40].ToString().Trim(),
                    ReWash = row[41].ToString().Trim(),
                    ReTolls = row[42].ToString().Trim(),
                    ReRepairs = row[43].ToString().Trim(),
                    ReMaintenance = row[44].ToString().Trim(),
                    ReHotel = row[45].ToString().Trim(),
                    ReWTickets = row[46].ToString().Trim(),
                    ReOil = row[47].ToString().Trim(),
                    ReTires = row[48].ToString().Trim(),
                    ReBonus = row[49].ToString().Trim(),
                    ReOwnRepairs = row[50].ToString().Trim(),
                    ReOwnMaintenance = row[51].ToString().Trim(),
                    ReParking = row[52].ToString().Trim(),
                    ReIdleAir = row[53].ToString().Trim(),
                    ReFees = row[54].ToString().Trim(),
                    ReOthers = row[55].ToString().Trim(),
                    SubTotal5 = row[56].ToString().Trim(),
                    AccidentalInsurance = row[57].ToString().Trim(),
                    OoPackage = row[58].ToString().Trim(),
                    ChildSupport = row[59].ToString().Trim(),
                    Insurance = row[60].ToString().Trim(),
                    Yard = row[61].ToString().Trim(),
                    DotComplaince = row[62].ToString().Trim(),
                    Escrow = row[63].ToString().Trim(),
                    CoDriverPay = row[64].ToString().Trim(),
                    OoAdvance = row[65].ToString().Trim(),
                    OoAdvanceCoDriver = row[66].ToString().Trim(),
                    OdOthers = row[67].ToString().Trim(),
                    SubTotal6 = row[68].ToString().Trim(),
                    Signs = row[69].ToString().Trim(),
                    Simplex = row[70].ToString().Trim(),
                    FinePenalty = row[71].ToString().Trim(),
                    CheckAmount = row[72].ToString().Trim(),
                    Address = row[73].ToString().Trim(),
                    City = row[74].ToString().Trim(),
                    State = row[75].ToString().Trim(),
                    Zip = row[76].ToString().Trim(),
                    Country = row[77].ToString().Trim(),
                    DriverType = row[78].ToString().Trim(),
                    TransactionType = row.Length > 79 ? row[79].ToString().Trim() : "",
                    Maintenance = row.Length > 80 ? row[80].ToString().Trim() : "0"
                };
                writeableRepository.Create(driverItem);
            }
        }

        private static void ImportCsvFile(IReadOnlyRepository readOnlyRepository, IWriteableRepository writeableRepository,
            Stream file, User user)
        {
            var reader = new StreamReader(file);

            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (line == null) continue;
                var row = line.Split(';');
                if (row[0] == "DatabaseName") continue;
                var invoiceCheck =
                    readOnlyRepository.Query<Invoice>(x => x.SosInvoiceNumber == row[7].ToString())
                        .FirstOrDefault();
                if (invoiceCheck != null)
                {
                    invoiceCheck.MarkAsUpdate = true;
                    writeableRepository.Update(invoiceCheck);
                    continue;
                }

                var invoiceItem = new Invoice()
                {
                    User = user,
                    DatabaseName = row[0].ToString().Trim(),
                    BrokerName = row[1].ToString().Trim(),
                    BillingAddress = row[2].ToString().Trim(),
                    City = row[3].ToString().Trim(),
                    State = row[4].ToString().Trim(),
                    ZipCode = row[5].ToString().Trim(),
                    Country = row[6].ToString().Trim(),
                    SosInvoiceNumber = row[7].ToString().Trim(),
                    LoadNumber = row[8].ToString().Trim(),
                    InvoiceDate = row[9].ToString().Trim(),
                    DueByDate = row[10].ToString().Trim(),
                    CustomerReferenceNumber = row[11].ToString().Trim(),
                    PickUpDate = row[12].ToString().Trim(),
                    InternalReferenceNumber = row[13].ToString().Trim(),
                    PickUpCity = row[14].ToString().Trim(),
                    DeliveryDate = row[15].ToString().Trim(),
                    DeliveryCity = row[16].ToString().Trim(),
                    PCs = row[17].ToString().Trim(),
                    Weight = row[18].ToString().Trim(),
                    Miles = row[19].ToString().Trim(),
                    Notes = row[20].ToString().Trim(),
                    BasicFreightRate = row[21].ToString().Trim(),
                    ExtraPickUp = row[22].ToString().Trim(),
                    ExtraDelivery = row[23].ToString().Trim(),
                    WaitingTime = row[24].ToString().Trim(),
                    LumperFees = row[25].ToString().Trim(),
                    Overnight = row[26].ToString().Trim(),
                    Others = row[27].ToString().Trim(),
                    FuelSurcharge = row[28].ToString().Trim(),
                    CongestionFee = row[29].ToString().Trim(),
                    BondFee = row[30].ToString().Trim(),
                    BrokerAdvance = row[31].ToString().Trim(),
                    BrokerAdvanceFee = row[32].ToString().Trim(),
                    Discount = row[33].ToString().Trim(),
                    TotalAmount = row[34].ToString().Trim(),
                    FootNotes = row[35].ToString().Trim(),
                    DispatcherName = row[36].ToString().Trim(),
                    RestakingFee = row.Length > 37 ? row[37].ToString().Trim() : "",
                    PortEntry = row.Length > 38 ? row[38].ToString().Trim() : "",
                    Overweight = "0",
                    PerDiem = "0",
                    BorderFee = "0",
                    Hazardous = "0",
                    QuickPayFee = "0",
                    Class = "0"
                };
                writeableRepository.Create(invoiceItem);
            }
        }

        private static void EditImportedInvoice(IWriteableRepository writeableRepository, Invoice invoiceCheck, DataRow row)
        {
            if (invoiceCheck.QbId != null)
            {
                invoiceCheck.MarkAsUpdate = true;
            }
            invoiceCheck.DatabaseName = row[0].ToString().Trim();
            invoiceCheck.BrokerName = row[1].ToString().Trim();
            invoiceCheck.BillingAddress = row[2].ToString().Trim();
            invoiceCheck.City = row[3].ToString().Trim();
            invoiceCheck.State = row[4].ToString().Trim();
            invoiceCheck.ZipCode = row[5].ToString().Trim();
            invoiceCheck.Country = row[6].ToString().Trim();
            invoiceCheck.SosInvoiceNumber = row[7].ToString().Trim();
            invoiceCheck.LoadNumber = row[8].ToString().Trim();
            invoiceCheck.InvoiceDate = row[9].ToString().Trim();
            invoiceCheck.DueByDate = row[10].ToString().Trim();
            invoiceCheck.CustomerReferenceNumber = row[11].ToString().Trim();
            invoiceCheck.PickUpDate = row[12].ToString().Trim();
            invoiceCheck.InternalReferenceNumber = row[13].ToString().Trim();
            invoiceCheck.PickUpCity = row[14].ToString().Trim();
            invoiceCheck.DeliveryDate = row[15].ToString().Trim();
            invoiceCheck.DeliveryCity = row[16].ToString().Trim();
            invoiceCheck.PCs = row[17].ToString().Trim();
            invoiceCheck.Weight = row[18].ToString().Trim();
            invoiceCheck.Miles = row[19].ToString().Trim();
            invoiceCheck.Notes = row[20].ToString().Trim();
            invoiceCheck.BasicFreightRate = row[21].ToString().Trim();
            invoiceCheck.ExtraPickUp = row[22].ToString().Trim();
            invoiceCheck.ExtraDelivery = row[23].ToString().Trim();
            invoiceCheck.WaitingTime = row[24].ToString().Trim();
            invoiceCheck.LumperFees = row[25].ToString().Trim();
            invoiceCheck.Overnight = row[26].ToString().Trim();
            invoiceCheck.Others = row[27].ToString().Trim();
            invoiceCheck.FuelSurcharge = row[28].ToString().Trim();
            invoiceCheck.CongestionFee = row[29].ToString().Trim();
            invoiceCheck.BondFee = row[30].ToString().Trim();
            invoiceCheck.BrokerAdvance = row[31].ToString().Trim();
            invoiceCheck.BrokerAdvanceFee = row[32].ToString().Trim();
            invoiceCheck.Discount = row[33].ToString().Trim();
            invoiceCheck.TotalAmount = row[34].ToString().Trim();
            invoiceCheck.FootNotes = row[35].ToString().Trim();
            invoiceCheck.DispatcherName = row[36].ToString().Trim();
            invoiceCheck.Overweight = row[37].ToString().Trim();
            invoiceCheck.PerDiem = row[38].ToString().Trim();
            invoiceCheck.BorderFee = row[39].ToString().Trim();
            invoiceCheck.Hazardous = row[40].ToString().Trim();
            invoiceCheck.QuickPayFee = row[41].ToString().Trim();
            invoiceCheck.Class = row[42].ToString().Trim();
            writeableRepository.Update(invoiceCheck);
        }

        private static void InsertNewInvoice(IWriteableRepository writeableRepository, User user, DataRow row)
        {
            var invoiceItem = new Invoice()
            {
                User = user,
                MarkAsUpdate = false,
                DatabaseName = row[0].ToString().Trim(),
                BrokerName = row[1].ToString().Trim(),
                BillingAddress = row[2].ToString().Trim(),
                City = row[3].ToString().Trim(),
                State = row[4].ToString().Trim(),
                ZipCode = row[5].ToString().Trim(),
                Country = row[6].ToString().Trim(),
                SosInvoiceNumber = row[7].ToString().Trim(),
                LoadNumber = row[8].ToString().Trim(),
                InvoiceDate = row[9].ToString().Trim(),
                DueByDate = row[10].ToString().Trim(),
                CustomerReferenceNumber = row[11].ToString().Trim(),
                PickUpDate = row[12].ToString().Trim(),
                InternalReferenceNumber = row[13].ToString().Trim(),
                PickUpCity = row[14].ToString().Trim(),
                DeliveryDate = row[15].ToString().Trim(),
                DeliveryCity = row[16].ToString().Trim(),
                PCs = row[17].ToString().Trim(),
                Weight = row[18].ToString().Trim(),
                Miles = row[19].ToString().Trim(),
                Notes = row[20].ToString().Trim(),
                BasicFreightRate = row[21].ToString().Trim(),
                ExtraPickUp = row[22].ToString().Trim(),
                ExtraDelivery = row[23].ToString().Trim(),
                WaitingTime = row[24].ToString().Trim(),
                LumperFees = row[25].ToString().Trim(),
                Overnight = row[26].ToString().Trim(),
                Others = row[27].ToString().Trim(),
                FuelSurcharge = row[28].ToString().Trim(),
                CongestionFee = row[29].ToString().Trim(),
                BondFee = row[30].ToString().Trim(),
                BrokerAdvance = row[31].ToString().Trim(),
                BrokerAdvanceFee = row[32].ToString().Trim(),
                Discount = row[33].ToString().Trim(),
                TotalAmount = row[34].ToString().Trim(),
                FootNotes = row[35].ToString().Trim(),
                DispatcherName = row[36].ToString().Trim(),
                Overweight = row[37].ToString().Trim(),
                PerDiem = row[38].ToString().Trim(),
                BorderFee = row[39].ToString().Trim(),
                Hazardous = row[40].ToString().Trim(),
                QuickPayFee = row[41].ToString().Trim(),
                Class = row[42].ToString().Trim()
            };
            writeableRepository.Create(invoiceItem);
        }

        private static string GetFtpUrl()
        {
            string url =
                (Environment.GetEnvironmentVariable("FtpUrlInvoice")
                 ?? ConfigurationManager.AppSettings["FtpUrlInvoice"]);
            return url;
        }

        private static string GetFtpUsername()
        {
            string url =
                (Environment.GetEnvironmentVariable("FtpUsername")
                 ?? ConfigurationManager.AppSettings["FtpUsername"]);
            return url;
        }

        private static string GetFtpPassword()
        {
            string url =
                (Environment.GetEnvironmentVariable("FtpPassword")
                 ?? ConfigurationManager.AppSettings["FtpPassword"]);
            return url;
        }
    }
}