﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Xml;
using DevDefined.OAuth.Consumer;
using DevDefined.OAuth.Framework;
using DevDefined.OAuth.Storage.Basic;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.LinqExtender;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.Security;
using Nancy;
using Nancy.ModelBinding;
using NHibernate.Criterion;
using QuickbooksIntegration.Domain.Entities;
using QuickbooksIntegration.Domain.Services;
using QuickbooksIntegration.Web.Api.Requests.Admin;
using Invoice = Intuit.Ipp.Data.Invoice;
using User = QuickbooksIntegration.Domain.Entities.User;

namespace QuickbooksIntegration.Web.Api.Modules
{
    public class QuickbooksConnectionModule : NancyModule
    {
        public static string RequestTokenUrl = ConfigurationManager.AppSettings["GET_REQUEST_TOKEN"];
        public static string AccessTokenUrl = ConfigurationManager.AppSettings["GET_ACCESS_TOKEN"];
        public static String AuthorizeUrl = ConfigurationManager.AppSettings["AuthorizeUrl"];
        public static String OauthUrl = ConfigurationManager.AppSettings["OauthLink"];
        public String ConsumerKey = ConfigurationManager.AppSettings["ConsumerKey"];
        public String ConsumerSecret = ConfigurationManager.AppSettings["ConsumerSecret"];
        public string StrrequestToken = string.Empty;
        public string TokenSecret = string.Empty;


        public QuickbooksConnectionModule(IReadOnlyRepository readOnlyRepository,
                    IWriteableRepository writeableRepository)
        {
            Get["/checkInvoices/{userEmail}/{invoiceNumber}/{id}"] =
                _ =>
                {
                    var userEmail = (string)_.userEmail;
                    var invoiceNumber = (string)_.invoiceNumber;
                    var id = (string)_.id;
                    var user = readOnlyRepository.Query<User>(x => x.Email == userEmail).First();
                    var reqValidator = new OAuthRequestValidator(user.AccessToken, user.AccessSecret, ConsumerKey, ConsumerSecret);

                    var context = new ServiceContext(user.RealmId, IntuitServicesType.QBO, reqValidator);
                    context.IppConfiguration.BaseUrl.Qbo = "https://quickbooks.api.intuit.com/";

                    var invoices = new QueryService<Invoice>(context).ExecuteIdsQuery("SELECT * FROM Invoice STARTPOSITION 1 MAXRESULTS 1000");
                    var invoiceReturn = invoices.Where(invoice => invoice.DocNumber == invoiceNumber || invoice.PONumber == invoiceNumber).Select(invoice => invoice.Id).FirstOrDefault();

                    return id + "," + invoiceReturn;
                };
            Post["/createDriverSettlement"] =
                _ =>
                {
                    var request = this.Bind<DriverSettlementRequest>();
                    var user = readOnlyRepository.Query<User>(x => x.Email == request.UserEmail).First();
                    var reqValidator = new OAuthRequestValidator(user.AccessToken, user.AccessSecret, ConsumerKey, ConsumerSecret);
                    var context = new ServiceContext(user.RealmId, IntuitServicesType.QBO, reqValidator);
                    context.IppConfiguration.BaseUrl.Qbo = "https://quickbooks.api.intuit.com/";
                    var service = new DataService(context);
                    var purchaseQuery = "SELECT * FROM Purchase where DocNumber = '" + request.FileNumber + "-E' STARTPOSITION 1 MAXRESULTS 1000";
                    var driverSettlementExpense = new QueryService<Purchase>(context)
                        .ExecuteIdsQuery(purchaseQuery)
                        .FirstOrDefault();

                    var dbDriverSettlement = readOnlyRepository.Query<DriverSettlement>(x => x.FileNumber == request.FileNumber).First();

                    if (string.IsNullOrEmpty(dbDriverSettlement.QbId))
                    {
                        if (driverSettlementExpense == null) {
                            Purchase newDriverSettlement;
                            if (request.TransactionType == "Check")
                            {
                                newDriverSettlement = CreateDriverSettlementServiceCheck(request, service, context);
                            }
                            else
                            {
                                newDriverSettlement = CreateDriverSettlementService(request, service, context);
                            }
                            if (newDriverSettlement != null)
                            {
                                dbDriverSettlement.QbId = newDriverSettlement.Id;
                                dbDriverSettlement.SyncDate = DateTime.Now.ToString("d/M/yyyy HH:mm:ss");
                                dbDriverSettlement.MarkAsUpdate = false;
                                writeableRepository.Update(dbDriverSettlement);
                            }
                            else
                            {
                                dbDriverSettlement.MarkAsUpdate = false;
                                dbDriverSettlement.Status = "Error";
                                writeableRepository.Update(dbDriverSettlement);
                            }
                        }
                        else {
                            dbDriverSettlement.MarkAsUpdate = false;
                            dbDriverSettlement.Status = "Exist";
                            writeableRepository.Update(dbDriverSettlement);
                        }

                    }
                    return null;
                };
            Get["/testQuicbooks"] =
                _ =>
                {
                    Console.Write("");
                    return null;
                };
            Post["/createInvoice"] =
                _ =>
                {
                    var request = this.Bind<InvoiceRequest>();
                    var user = readOnlyRepository.Query<User>(x => x.Email == request.UserEmail).First();
                    var reqValidator = new OAuthRequestValidator(user.AccessToken, user.AccessSecret, ConsumerKey, ConsumerSecret);
                    var context = new ServiceContext(user.RealmId, IntuitServicesType.QBO, reqValidator);
                    context.IppConfiguration.BaseUrl.Qbo = "https://quickbooks.api.intuit.com/";
                    var service = new DataService(context);
                    var invoices = new QueryService<Invoice>(context).ExecuteIdsQuery("SELECT * FROM Invoice where DocNumber = '" + request.SOSInvoiceNumber + "' STARTPOSITION 1 MAXRESULTS 1000");
                    
                    var data = invoices.FirstOrDefault();
                    var dbInvoice = readOnlyRepository.Query<Domain.Entities.Invoice>(x => x.SosInvoiceNumber == request.SOSInvoiceNumber).First();

                    if (data == null)
                    {
                        if (string.IsNullOrEmpty(dbInvoice.QbId) && string.IsNullOrEmpty(dbInvoice.Status))
                        {
                            dbInvoice.InSyncProcess = true;
                            writeableRepository.Update(dbInvoice);
                            var invoice = CreateInvoiceService(request, service, context);
                            dbInvoice.QbId = invoice.Id;
                            dbInvoice.InSyncProcess = false;
                            dbInvoice.SyncDate = DateTime.Now.ToString("d/M/yyyy HH:mm:ss");
                            dbInvoice.MarkAsUpdate = false;
                            writeableRepository.Update(dbInvoice);
                        }
                    } else
                    {
                        dbInvoice.MarkAsUpdate = false;
                        dbInvoice.InSyncProcess = false;
                        dbInvoice.Status = "Exist";
                        writeableRepository.Update(dbInvoice);
                    }
                    
                    return null;
                };
            Post["/createInvoices"] =
                _ =>
                {
                    var request = this.Bind<InvoicesRequest>();
                    var user = readOnlyRepository.Query<User>(x => x.Email == request.UserEmail).First();
                    var reqValidator = new OAuthRequestValidator(user.AccessToken, user.AccessSecret, ConsumerKey, ConsumerSecret);
                    var context = new ServiceContext(user.RealmId, IntuitServicesType.QBO, reqValidator);
                    context.IppConfiguration.BaseUrl.Qbo = "https://quickbooks.api.intuit.com/";
                    var service = new DataService(context);
                    foreach (var invoice in request.Invoices)
                    {
                        CreateInvoiceService(invoice, service, context);    
                    }
                    
                    return null;
                };
            Get["/isQuickbookAssociated/{userEmail}"] =
                _ =>
                {
                    var userEmail = (string) _.userEmail;
                    var user = readOnlyRepository.Query<User>(x => x.Email == userEmail).First();
                    var isQuickbookAssociated = !string.IsNullOrEmpty(user.OauthToken);
                    return isQuickbookAssociated;
                };
            Post["/saveQuickbookToken"] =
                _ =>
                {
                    var request = this.Bind<OauthTokenRequest>();
                    var user = readOnlyRepository.Query<User>(x => x.Email == request.UserEmail).First();
                    user.OauthToken = request.OauthToken;
                    user.OauthVerifier = request.OauthVerifier;
                    user.RealmId = request.RealmId;
                    user.DataSource = request.DataSource;

                    var oauth = GetAuthContext();
                    IToken token = new RequestToken()
                    {
                        Token = user.RequestToken,
                        TokenSecret = user.RequestTokenSecret
                    };
                    var accessToken = oauth.ExchangeRequestTokenForAccessToken(token, user.OauthVerifier);
                    user.AccessToken = accessToken.Token;
                    user.AccessSecret = accessToken.TokenSecret;

                    writeableRepository.Update(user);
                    return null;
                };
        }
        private static Line AddNewExpenseItemLine(decimal amount, string itemId, string description)
        {
            var expenseItem =
                new Line
                {
                    DetailType = LineDetailTypeEnum.AccountBasedExpenseLineDetail,
                    Amount = amount,
                    Description = description,
                    AmountSpecified = true,
                    DetailTypeSpecified = true,
                    
                    AnyIntuitObject = new AccountBasedExpenseLineDetail()
                    {
                        AccountRef = new ReferenceType
                        {
                            Value = itemId
                        },
                        BillableStatus = BillableStatusEnum.NotBillable,
                        TaxCodeRef = new ReferenceType() { Value = "NON" }
                    }
                };
            return expenseItem;
        }

        private static string GetExpenseItemId(ServiceContext context, IDataService service, string parentExpenseName, string expenseName)
        {
            var expenseSubAccountId = "";
            var expenseSubAccountList = new QueryService<Account>(context)
                .ExecuteIdsQuery("SELECT * FROM Account where Name = '" + expenseName + "' STARTPOSITION 1 MAXRESULTS 1000")
                .ToList();

            var expenseSubAccount = expenseSubAccountList.FirstOrDefault();

            if (expenseSubAccount != null)
            {
                expenseSubAccountId = expenseSubAccount.Id;
            }
            else
            {
                var expenseAccountId = "";
                var expenseAccountList = new QueryService<Account>(context)
                    .ExecuteIdsQuery("SELECT * FROM Account where Name = '" + parentExpenseName + "' STARTPOSITION 1 MAXRESULTS 1000")
                    .ToList();
                var expenseAccount = expenseAccountList.FirstOrDefault();
                if (expenseAccount != null)
                {
                    expenseAccountId = expenseAccount.Id;
                    if (string.IsNullOrEmpty(expenseName))
                    {
                        return expenseAccountId;
                    }
                }
                else
                {
                    var parentAccount = new Account()
                    {
                        AccountType = AccountTypeEnum.Expense,
                        Classification = AccountClassificationEnum.Expense,
                        AccountSubType = "OtherMiscellaneousServiceCost",
                        CurrentBalance = 0,
                        CurrentBalanceWithSubAccounts = 0,
                        Active = true,
                        Name = parentExpenseName,
                        SubAccount = false,
                        SubAccountSpecified = true
                    };
                    var newParentAccount = service.Add(parentAccount);
                    expenseAccountId = newParentAccount.Id;
                    if (string.IsNullOrEmpty(expenseName))
                    {
                        return expenseAccountId;
                    }
                }
                var subAccount = new Account()
                {
                    AccountType = AccountTypeEnum.Expense,
                    Classification = AccountClassificationEnum.Expense,
                    AccountSubType = "OtherMiscellaneousServiceCost",
                    CurrentBalance = 0,
                    CurrentBalanceWithSubAccounts = 0,
                    Active = true,
                    Name = expenseName,
                    SubAccount = true,
                    SubAccountSpecified = true,
                    ParentRef = new ReferenceType() { Value = expenseAccountId }
                };
                var newSubAccount = service.Add(subAccount);
                expenseSubAccountId = newSubAccount.Id;
            }

            return expenseSubAccountId;
        }

        private static List<Line> GetExpenseLines(DriverSettlementRequest request, IDataService service, ServiceContext context)
        {
            var lines = new List<Line>();
            string driverPaySubAccountId = "";
            string driverPayAccountId = "";
            if (request.BasicDriverPay != 0 || request.WaitingTime != 0 || request.ExtraPickUp != 0 || request.ExtraDelivery != 0 ||
                request.Overnight != 0 || request.Others != 0 || request.ExtraMiles != 0 || request.EmptyMileDebit != 0 ||
                request.FuelSurcharge != 0)
            {
                driverPaySubAccountId =
                GetExpenseItemId(context, service, "Direct Operating Expenses", request.DriverType == "OO" ? "Flat Rate" : "Basic Driver Pay");
                if (request.BasicDriverPay != 0) { lines.Add(AddNewExpenseItemLine(request.BasicDriverPay, driverPaySubAccountId, "Basic Driver Pay, File #: " + request.FileNumber)); }
                if (request.WaitingTime != 0) { lines.Add(AddNewExpenseItemLine(request.WaitingTime, driverPaySubAccountId, "Waiting Time, File #: " + request.FileNumber)); }
                if (request.ExtraPickUp != 0) { lines.Add(AddNewExpenseItemLine(request.ExtraPickUp, driverPaySubAccountId, "Extra Pickup, File #: " + request.FileNumber)); }
                if (request.ExtraDelivery != 0) { lines.Add(AddNewExpenseItemLine(request.ExtraDelivery, driverPaySubAccountId, "Extra Delivery, File #: " + request.FileNumber)); }
                if (request.Overnight != 0) { lines.Add(AddNewExpenseItemLine(request.Overnight, driverPaySubAccountId, "Overnight, File #: " + request.FileNumber)); }
                if (request.Others != 0) { lines.Add(AddNewExpenseItemLine(request.Others, driverPaySubAccountId, "Others, File #: " + request.FileNumber)); }
                if (request.ExtraMiles != 0) { lines.Add(AddNewExpenseItemLine(request.ExtraMiles, driverPaySubAccountId, "Extra Miles, File #: " + request.FileNumber)); }
                if (request.EmptyMileDebit != 0) { lines.Add(AddNewExpenseItemLine(request.EmptyMileDebit, driverPaySubAccountId, "Empty Miles Debit, File #: " + request.FileNumber)); }
                if (request.FuelSurcharge != 0) { lines.Add(AddNewExpenseItemLine(request.FuelSurcharge, driverPaySubAccountId, "Fuel Surcharge, File #: " + request.FileNumber)); }
            }

            if (request.OoAdvanceCoDriver != 0 || request.OoAdvance != 0) {
                driverPayAccountId = GetExpenseItemId(context, service, "Direct Operating Expenses", "Basic Driver Pay");
                if (request.OoAdvanceCoDriver != 0) { lines.Add(AddNewExpenseItemLine(request.OoAdvanceCoDriver, driverPayAccountId, "OO Advance CoDriver, File #: " + request.FileNumber)); }
                if (request.OoAdvance != 0) { lines.Add(AddNewExpenseItemLine(request.OoAdvance, driverPayAccountId, "OO Advance, File #: " + request.FileNumber)); }
            }

            string dieselSubAccountId = "" ;
            if (request.Fuel != 0 || request.FuelFee != 0 || request.RefFuel != 0 || request.FuelRebate != 0 ||
                request.DriverCash != 0  || request.ReDiesel != 0)
            {
                dieselSubAccountId = GetExpenseItemId(context, service, "Direct Operating Expenses", "Diesel");
                if (request.DriverType == "OO")
                {
                    if (request.Fuel != 0) { lines.Add(AddNewExpenseItemLine(request.Fuel, dieselSubAccountId, "Fuel")); }
                    if (request.FuelFee != 0) { lines.Add(AddNewExpenseItemLine(request.FuelFee, dieselSubAccountId, "Fuel Fee")); }
                    if (request.RefFuel != 0) { lines.Add(AddNewExpenseItemLine(request.RefFuel, dieselSubAccountId, "Ref. Fuel")); }
                    if (request.FuelRebate != 0) { lines.Add(AddNewExpenseItemLine(request.FuelRebate, dieselSubAccountId, "Fuel Rebate")); }
                    if (request.DriverCash != 0) { lines.Add(AddNewExpenseItemLine(request.DriverCash, dieselSubAccountId, "Driver Cash")); }
                }

                if (request.ReDiesel != 0) { lines.Add(AddNewExpenseItemLine(request.ReDiesel, dieselSubAccountId, "Diesel")); }
            }

            string cashAdvancesSubAccountId = "";
            if (request.TruckLease != 0 || request.Loan != 0 || request.LoanFee != 0 || request.RoadTax != 0 ||
                request.TrailerLease != 0 || request.LicensePlate != 0)
            {
                cashAdvancesSubAccountId = GetExpenseItemId(context, service, "Cash Advances", "");
                if (request.TruckLease != 0) { lines.Add(AddNewExpenseItemLine(request.TruckLease, cashAdvancesSubAccountId, "Truck Lease")); }
                if (request.Loan != 0) { lines.Add(AddNewExpenseItemLine(request.Loan, cashAdvancesSubAccountId, "Loan")); }
                if (request.LoanFee != 0) { lines.Add(AddNewExpenseItemLine(request.LoanFee, cashAdvancesSubAccountId, "Loan Fee")); }
                if (request.RoadTax != 0) { lines.Add(AddNewExpenseItemLine(request.RoadTax, cashAdvancesSubAccountId, "Road Tax")); }
                if (request.TrailerLease != 0) { lines.Add(AddNewExpenseItemLine(request.TrailerLease, cashAdvancesSubAccountId, "Trailer Lease")); }
                if (request.LicensePlate != 0) { lines.Add(AddNewExpenseItemLine(request.LicensePlate, cashAdvancesSubAccountId, "License Plate")); }
            }

            if (request.TrailerRent != 0)
            {
                var trailRentalSubAccountId = GetExpenseItemId(context, service, "Rental Income", "Trailer Rental");
                lines.Add(AddNewExpenseItemLine(request.TrailerRent, trailRentalSubAccountId, "Trailer Rent"));
            }

            if (request.Yard != 0)
            {
                var yardRentalSubAccountId = GetExpenseItemId(context, service, "Rental Income", "Yard Rental");
                lines.Add(AddNewExpenseItemLine(request.Yard, yardRentalSubAccountId, "Yard"));
            }

            if (request.Insurance != 0 || request.AccidentalInsurance != 0)
            {
                var truckInsuranceSubAccountId = GetExpenseItemId(context, service, "Insurance", "Truck Insurance");
                if (request.Insurance != 0) { lines.Add(AddNewExpenseItemLine(request.Insurance, truckInsuranceSubAccountId, "Insurance")); }
                if (request.AccidentalInsurance != 0) { lines.Add(AddNewExpenseItemLine(request.AccidentalInsurance, truckInsuranceSubAccountId, "Accidental Insurance")); }
            }

            if (request.ReOthers != 0 && request.ChildSupport != 0 || request.Escrow != 0 && request.CoDriverPay != 0 ||
                request.NegativeFile != 0 && request.QuickPayFee != 0 || request.QuickPayPercent != 0 && request.OoPackage != 0 ||
                request.DotComplaince != 0) {
                var miscellaneousSubAccountId = GetExpenseItemId(context, service, "Other Miscellaneous Expense", "");
                if (request.ReOthers != 0) { lines.Add(AddNewExpenseItemLine(request.ReOthers, miscellaneousSubAccountId, "Re Others")); }
                if (request.ChildSupport != 0) { lines.Add(AddNewExpenseItemLine(request.ChildSupport, miscellaneousSubAccountId, "Child Support")); }
                if (request.Escrow != 0) { lines.Add(AddNewExpenseItemLine(request.Escrow, miscellaneousSubAccountId, "Escrow")); }
                if (request.CoDriverPay != 0) { lines.Add(AddNewExpenseItemLine(request.CoDriverPay, miscellaneousSubAccountId, "CoDriver Pay")); }
                if (request.NegativeFile != 0) { lines.Add(AddNewExpenseItemLine(request.NegativeFile, miscellaneousSubAccountId, "Negative Files")); }
                if (request.QuickPayFee != 0) { lines.Add(AddNewExpenseItemLine(request.QuickPayFee, miscellaneousSubAccountId, "Quick Pay Fee")); }
                if (request.QuickPayPercent != 0) { lines.Add(AddNewExpenseItemLine(request.QuickPayPercent, miscellaneousSubAccountId, "Quick Pay Percent")); }
                if (request.OoPackage != 0) { lines.Add(AddNewExpenseItemLine(request.OoPackage, miscellaneousSubAccountId, "OO Package")); }
                if (request.DotComplaince != 0) { lines.Add(AddNewExpenseItemLine(request.DotComplaince, miscellaneousSubAccountId, "Dot Complaince")); }
            }

            if (request.ReIdleAir > 0)
            {
                var efsAdvancesFee = GetExpenseItemId(context, service, "EFS Advances Fee", "");
                lines.Add(AddNewExpenseItemLine(request.ReIdleAir, efsAdvancesFee, "Advance Fee"));
            }

            if (request.EfsCashAdvances != 0 || request.EfsFee != 0 || request.ReFees != 0 || request.WesternUnion != 0 ||
                request.OwnChecks != 0 || request.Transcheck != 0 || request.AdvanceFee != 0) {
                var efsAdvanceSubAccountId = GetExpenseItemId(context, service, "EFS Advances Account", "");
                if (request.EfsCashAdvances != 0) { lines.Add(AddNewExpenseItemLine(request.EfsCashAdvances, efsAdvanceSubAccountId, "EFS Cash Advances")); }
                if (request.EfsFee != 0) { lines.Add(AddNewExpenseItemLine(request.EfsFee, efsAdvanceSubAccountId, "EFS Fee")); }
                if (request.ReFees != 0) { lines.Add(AddNewExpenseItemLine(request.ReFees, efsAdvanceSubAccountId, "Fees")); }
                if (request.WesternUnion != 0) { lines.Add(AddNewExpenseItemLine(request.WesternUnion, efsAdvanceSubAccountId, "Western Union")); }
                if (request.OwnChecks != 0) { lines.Add(AddNewExpenseItemLine(request.OwnChecks, efsAdvanceSubAccountId, "Own Checks")); }
                if (request.Transcheck != 0) { lines.Add(AddNewExpenseItemLine(request.Transcheck, efsAdvanceSubAccountId, "Trans Check")); }
                if (request.AdvanceFee != 0) { lines.Add(AddNewExpenseItemLine(request.AdvanceFee, efsAdvanceSubAccountId, "Advance Fee")); }
            }

            if (request.LumperFees > 0)
            {
                var lumperSubAccountId = GetExpenseItemId(context, service, "Direct Operating Expenses", "Lumper");
                lines.Add(AddNewExpenseItemLine(request.LumperFees, lumperSubAccountId, "Lumper Fee"));
            }
            if (request.ReParking > 0)
            {
                var repairSubAccountId = GetExpenseItemId(context, service, "Direct Operating Expenses", "Parking Expense");
                if (request.ReParking > 0) { lines.Add(AddNewExpenseItemLine(request.LumperFees, repairSubAccountId, "Parking Expense")); }
            }
            if (request.ReWash > 0)
            {
                var washExpenseSubAccountId = GetExpenseItemId(context, service, "Direct Operating Expenses", "Wash Expense");
                if (request.ReWash > 0) { lines.Add(AddNewExpenseItemLine(request.ReWash, washExpenseSubAccountId, "Wash Expense")); }
            }
     
            if (request.ReTolls > 0 || request.OdOthers != 0)
            {
                var tollsExpenseSubAccountId = GetExpenseItemId(context, service, "Direct Operating Expenses", "Tolls Expense");
                if (request.ReTolls > 0) { lines.Add(AddNewExpenseItemLine(request.ReTolls, tollsExpenseSubAccountId, "Tolls")); }
                if (request.OdOthers != 0) { lines.Add(AddNewExpenseItemLine(request.OdOthers, tollsExpenseSubAccountId, "OD Others")); }
            }
            if (request.Maintenance != 0)
            {
                var truckMaintenanceCostId = GetExpenseItemId(context, service, "Truck Maintenance Costs", "");
                if (request.Maintenance != 0) { lines.Add(AddNewExpenseItemLine(request.Maintenance, truckMaintenanceCostId, "Maintenance")); }
            }
            if (request.ReRepairs > 0 || request.ReMaintenance > 0 || request.ReOil > 0 || request.ReTires > 0 ||
                request.ReOwnRepairs > 0 || request.ReOwnMaintenance > 0)
            {
                var partsAndRepairSubAccountId = GetExpenseItemId(context, service, "Direct Operating Expenses", "Parts & Repair Expense");
                if (request.ReRepairs > 0) { lines.Add(AddNewExpenseItemLine(request.ReRepairs, partsAndRepairSubAccountId, "Repairs")); }
                if (request.ReMaintenance > 0) { lines.Add(AddNewExpenseItemLine(request.ReMaintenance, partsAndRepairSubAccountId, "Maintenance")); }
                if (request.ReOil > 0) { lines.Add(AddNewExpenseItemLine(request.ReOil, partsAndRepairSubAccountId, "Oil")); }
                if (request.ReTires > 0) { lines.Add(AddNewExpenseItemLine(request.ReTires, partsAndRepairSubAccountId, "Tires")); }
                if (request.ReOwnRepairs > 0) { lines.Add(AddNewExpenseItemLine(request.ReOwnRepairs, partsAndRepairSubAccountId, "Own Repairs")); }
                if (request.ReOwnMaintenance > 0) { lines.Add(AddNewExpenseItemLine(request.ReOwnMaintenance, partsAndRepairSubAccountId, "Own Maintenance")); }
            }
            if (request.ReHotel > 0)
            {
                var travelSubAccountId = GetExpenseItemId(context, service, "Admin & General Expense", "Travel");
                if (request.ReHotel > 0) { lines.Add(AddNewExpenseItemLine(request.ReHotel, travelSubAccountId, "Travel")); }
            }

            if (request.ReWTickets > 0)
            {
                var ticketsSubAccountId = GetExpenseItemId(context, service, "Direct Operating Expenses", "Tickets");
                if (request.ReWTickets > 0) { lines.Add(AddNewExpenseItemLine(request.ReWTickets, ticketsSubAccountId, "Tickets")); }
            }

            if (request.ReBonus > 0)
            {
                var bonusSubAccountId = GetExpenseItemId(context, service, "Payroll Expenses", "Bonus");
                if (request.ReBonus > 0) { lines.Add(AddNewExpenseItemLine(request.ReBonus, bonusSubAccountId, "Bonus")); }
            }

            return lines;
        }
        private static string GetVendor(ServiceContext context, DriverSettlementRequest request, DataService service)
        {
            string selectedEntityId;
            string searchField;
            var vendorName = request.DriverType == "OO" ? request.TruckOwner : request.Driver;
            if (vendorName.Contains("'"))
            {

                searchField = vendorName.Replace("'", "\\'");
            } else
            {
                searchField = vendorName;
            }
            var vendor = new QueryService<Vendor>(context)
                .ExecuteIdsQuery("SELECT * FROM Vendor where DisplayName = '" + searchField + "' STARTPOSITION 1 MAXRESULTS 1000")
                .FirstOrDefault();

            if (vendor == null)
            {
                var newVendor = new Vendor()
                {
                    DisplayName = vendorName
                };
                var vendorItem = service.Add(newVendor);
                selectedEntityId = vendorItem.Id;
            }
            else
            {
                selectedEntityId = vendor.Id;
            }
            
            return selectedEntityId;
        }
        private static string GetAccount(ServiceContext context)
        {
            var account = new QueryService<Account>(context)
                .ExecuteIdsQuery("SELECT * FROM Account where Name = 'PLAT BUS CHECKING (9289)' STARTPOSITION 1 MAXRESULTS 1000")
                .FirstOrDefault();

            var selectedEntityId = account == null ? "" : account.Id;

            return selectedEntityId;
        }
        private static string GetPaymentMethod(ServiceContext context, string paymentType)
        {
            var type = paymentType == "Check" ? paymentType : "Cash";
            var paymentMethod = new QueryService<PaymentMethod>(context)
                .ExecuteIdsQuery("SELECT * FROM PaymentMethod where Name = '" + paymentType + "' STARTPOSITION 1 MAXRESULTS 1000")
                .FirstOrDefault();

            var selectedEntityId = paymentMethod == null ? "" : paymentMethod.Id;

            return selectedEntityId;
        }
        private static string GetCustomer(ServiceContext context, InvoiceRequest request, DataService service)
        {
            var selectedCustomerId = "";
            var existCustomer = false;
            var customersPage1 = new QueryService<Customer>(context).ExecuteIdsQuery("SELECT * FROM Customer STARTPOSITION 1 MAXRESULTS 1000");
            var customersPage2 = new QueryService<Customer>(context).ExecuteIdsQuery("SELECT * FROM Customer STARTPOSITION 1000 MAXRESULTS 1000");
            var customers = customersPage1.Concat(customersPage2);
            //if (request.BrokerName.Length > 25)
            //{
            request.BrokerName = request.BrokerName.Trim().ToLower();
            //}

            foreach (var customer in customers)
            {
                if (!customer.DisplayName.ToLower().Contains(request.BrokerName)) continue;
                existCustomer = true;
                selectedCustomerId = customer.Id;
            }

            if (!existCustomer)
            {
                var entity = new Customer()
                {
                    DisplayName = request.BrokerName,
                    PrimaryEmailAddr = new EmailAddress()
                };
                var customer = service.Add(entity);
                selectedCustomerId = customer.Id;
            }
            return selectedCustomerId;
        }

        private static Purchase CreateDriverSettlementService(DriverSettlementRequest request,
            DataService service, ServiceContext context)
        {
            var expense = new Purchase()
            {
               
                PurchaseEx = new IntuitAnyType() { },
                domain = "QBO",
                //sparse = false,
                EntityRef = new ReferenceType() { Value = GetVendor(context, request, service) },
                AccountRef = new ReferenceType() { Value = GetAccount(context) },
                TxnDate = DateTime.Now,
                TxnDateSpecified = true,
                PaymentMethodRef = new ReferenceType() { Value = GetPaymentMethod(context, request.TransactionType) },
                PaymentType = request.TransactionType == "Check" ? PaymentTypeEnum.Check : PaymentTypeEnum.Cash,
                PaymentTypeSpecified = true,
                DocNumber = request.FileNumber + "-E",
                PaymentRefNum = request.FileNumber,
                Line = GetExpenseLines(request, service, context).ToArray(),
                RemitToAddr = new PhysicalAddress()
                {
                    City = request.City,
                    Country = request.Country,
                    Line1 = request.Address
                },
                TotalAmt = request.CheckAmount,
                TotalAmtSpecified = true,
                PrivateNote = "Drive: " + request.Driver + " - " +
                       "Driver Type: " + request.DriverType + " - " +
                       "Truck Number: " + request.TruckNumber,
                Memo = "Drive: " + request.Driver + " - " +
                       "Driver Type: " + request.DriverType + " - " +
                       "Truck Number: " + request.TruckNumber,
                PrintStatus = PrintStatusEnum.NeedToPrint,
                PrintStatusSpecified = true
            };
            if (expense.TotalAmt < 0)
            {
                return null;
            }
            try
            {
                var newExpense = service.Add(expense);
                return newExpense;
            } catch
            {
                return null;
            }
        }

        private static Purchase CreateDriverSettlementServiceCheck(DriverSettlementRequest request,
            DataService service, ServiceContext context)
        {
            var expense = new Purchase()
            {

                PurchaseEx = new IntuitAnyType() { },
                domain = "QBO",
                //sparse = false,
                EntityRef = new ReferenceType() { Value = GetVendor(context, request, service) },
                AccountRef = new ReferenceType() { Value = GetAccount(context) },
                TxnDate = DateTime.Now,
                TxnDateSpecified = true,
                PaymentMethodRef = new ReferenceType() { Value = GetPaymentMethod(context, request.TransactionType) },
                PaymentType = PaymentTypeEnum.Check,
                PaymentTypeSpecified = true,
                PaymentRefNum = request.FileNumber,
                Line = GetExpenseLines(request, service, context).ToArray(),
                RemitToAddr = new PhysicalAddress()
                {
                    City = request.City,
                    Country = request.Country,
                    Line1 = request.Address
                },
                TotalAmt = request.CheckAmount,
                TotalAmtSpecified = true,
                PrivateNote = "Drive: " + request.Driver + " - " +
                       "Driver Type: " + request.DriverType + " - " +
                       "Truck Number: " + request.TruckNumber,
                Memo = "Drive: " + request.Driver + " - " +
                       "Driver Type: " + request.DriverType + " - " +
                       "Truck Number: " + request.TruckNumber + " - " +
                       "File Number: " + request.FileNumber,
                PrintStatus = PrintStatusEnum.NeedToPrint,
                PrintStatusSpecified = true
            };
            if (expense.TotalAmt < 0)
            {
                return null;
            }
            var newExpense = service.Add(expense);
            return newExpense;
        }

        private static string GetSalesTerm(ServiceContext context, DataService service)
        {
            var selectedSalesTermId = "";
            var existSalesTerm = false;
            var terms = new QueryService<Term>(context).ExecuteIdsQuery("SELECT * FROM Term STARTPOSITION 1 MAXRESULTS 1000");
            foreach (var term in terms.Where(term => term.Name == "Net 30"))
            {
                existSalesTerm = true;
                selectedSalesTermId = term.Id;
            }
            if (!existSalesTerm)
            {
                var entity = new Term()
                {
                    Name = "Net 30"
                };
                service.Add(entity);
                var term = service.Add(entity);
                selectedSalesTermId = term.Id;
            }
            return selectedSalesTermId;
        }

        private static Invoice CreateInvoiceService(InvoiceRequest request, DataService service, ServiceContext context)
        {
            var selectedCustomerId = GetCustomer(context, request, service);
            var selectedSalesTermId = GetSalesTerm(context, service);
            var pickUpDate = Convert.ToDateTime(request.PickUpDate);
            var dueDate = Convert.ToDateTime(request.DueDate);
            var txnDate = Convert.ToDateTime(request.InvoiceDate);
            var lines = GetLines(request, service, context);

            var invoiceEntity = new Invoice
            {
                DocNumber = request.SOSInvoiceNumber,
                CustomField = new []
                {
                    new CustomField() {DefinitionId = "1", Name = "P.O. NUMBER", AnyIntuitObject = "0" + request.CustomerReferenceNumber, Type = CustomFieldTypeEnum.StringType},
                    new CustomField() {DefinitionId = "2", Name = "GLOBAL LOAD #", AnyIntuitObject = request.LoadNumber, Type = CustomFieldTypeEnum.StringType},
                },
                DueDate = new DateTime(dueDate.Year, dueDate.Month, dueDate.Day),
                TxnDate = new DateTime(txnDate.Year, txnDate.Month, txnDate.Day),
                DueDateSpecified = true,
                TxnDateSpecified = true,
                BillAddr = new PhysicalAddress()
                {
                    City = request.City,
                    Country = request.Country,
                    Line1 = request.BillingAddress
                },
                TotalAmt = request.TotalAmount,
                Line = lines.ToArray(),
                CustomerRef = new ReferenceType {Value = selectedCustomerId},
                SalesTermRef = new ReferenceType {Value = selectedSalesTermId},
                TxnTaxDetail = new TxnTaxDetail {TotalTax = 0},
                ApplyTaxAfterDiscount = false,
                PrintStatus = PrintStatusEnum.NeedToPrint,
                PrintStatusSpecified = true,
                EmailStatus = EmailStatusEnum.NotSet,
                EmailStatusSpecified = true,
                Deposit = 0,
                PrivateNote = "CustomerReferenceNumber: " + request.CustomerReferenceNumber + " - " +
                              " Pickup Date: " + pickUpDate.ToShortDateString() + " - " +
                              " Pickup City: " + request.PickUpCity + " - " +
                              " Delivery City: " + request.DeliveryCity,
                DepositSpecified = true,
                AllowIPNPayment = false,
                AllowIPNPaymentSpecified = true,
                AllowOnlinePayment = false,
                AllowOnlinePaymentSpecified = true,
                AllowOnlineCreditCardPayment = false,
                AllowOnlineCreditCardPaymentSpecified = true,
                AllowOnlineACHPayment = false,
                AllowOnlineACHPaymentSpecified = true,
            };
            return service.Add(invoiceEntity);
        }

        private static Invoice UpdateInvoiceService(InvoiceRequest request, DataService service, ServiceContext context, Invoice invoice)
        {
            var selectedCustomerId = GetCustomer(context, request, service);
            var selectedSalesTermId = GetSalesTerm(context, service);
            var pickUpDate = Convert.ToDateTime(request.PickUpDate);
            var dueDate = Convert.ToDateTime(request.DueDate);
            var txnDate = Convert.ToDateTime(request.InvoiceDate);
            var lines = GetLines(request, service, context);


            invoice.PONumber = request.SOSInvoiceNumber;
            invoice.AutoDocNumber = true;
            invoice.DueDate = new DateTime(dueDate.Year, dueDate.Month, dueDate.Day);
            invoice.DueDateSpecified = true;
            invoice.TxnDateSpecified = true;
            invoice.TxnDate = new DateTime(txnDate.Year, txnDate.Month, txnDate.Day);
            invoice.BillAddr = new PhysicalAddress()
            {
                City = request.City,
                Country = request.Country,
                Line1 = request.BillingAddress
            };
            invoice.TotalAmt = request.TotalAmount;
            // invoice.Line = lines.ToArray();
            invoice.CustomerRef = new ReferenceType {Value = selectedCustomerId};
            invoice.PrivateNote = "CustomerReferenceNumber: " + request.CustomerReferenceNumber +
                                  " Pickup Date: " + pickUpDate.ToShortDateString() +
                                  " Pickup City: " + request.PickUpCity +
                                  " Delivery City: " + request.DeliveryCity;
            return service.Update(invoice);
        }

        private static string GetItemId(ServiceContext context, IDataService service, string itemName)
        {
            var item = new QueryService<Item>(context).ExecuteIdsQuery("SELECT * FROM Item STARTPOSITION 1 MAXRESULTS 1000");
            var itemId = "";
            foreach (var serv in item.Where(serv => serv.Name.Contains(itemName))) { itemId = serv.Id; }
            if (itemId != "") return itemId;

            var income = new QueryService<Account>(context).ExecuteIdsQuery("SELECT * FROM Account STARTPOSITION 1 MAXRESULTS 1000");

            var incomesServices = "";
            foreach (var inc in income)
            {
                if (inc.Name == "Gross Income")
                {
                    incomesServices = inc.Id;
                    break;
                }
            }

            var entity = new Item()
            {
                Active = true,
                ActiveSpecified = true,
                TypeSpecified = true,
                Type = ItemTypeEnum.Service,
                TrackQtyOnHand = false,
                ItemCategoryType = "Service",
                Name = itemName,
                Description = itemName,
                
                IncomeAccountRef = new ReferenceType { Value = incomesServices }
            };
            var lineItem = service.Add(entity);
            itemId = lineItem.Id;
            return itemId;
        }

        private static Line AddNewSalesItemLine(decimal amount, string itemId)
        {
            var salesItem =
                new Line
                {
                    DetailType = LineDetailTypeEnum.SalesItemLineDetail,
                    Amount = amount,
                    AmountSpecified = true,
                    DetailTypeSpecified = true,
                    AnyIntuitObject = new SalesItemLineDetail
                    {
                        ItemRef = new ReferenceType
                        {
                            Value = itemId
                        }
                    }
                };
            return salesItem;
        }
        private static Line AddNewDiscountItemLine(decimal amount, string itemId)
        {
            var salesItem =
                new Line
                {
                    DetailType = LineDetailTypeEnum.DiscountLineDetail,
                    Amount = amount,
                    AmountSpecified = true,
                    DetailTypeSpecified = true,
                    AnyIntuitObject = new DiscountLineDetail()
                    {
                        DiscountRef = new ReferenceType
                        {
                            Value = itemId
                        }
                    }
                };
            return salesItem;
        }
        private static List<Line> GetLines(InvoiceRequest request, IDataService service, ServiceContext context)
        {
            var lines = new List<Line>();

            if (request.BasicFreightRate > 0 || request.ExtraPickUp > 0 || request.ExtraDelivery > 0 || request.WaitingTime > 0 ||
                request.Overnight > 0 || request.Others > 0 || request.BrokerAdvance > 0 || request.BrokerAdvanceFee > 0 || request.Discount > 0)
            {
                var fullLoadId = GetItemId(context, service, "Full Load");
                if (request.BasicFreightRate > 0) { lines.Add(AddNewSalesItemLine(request.BasicFreightRate, fullLoadId)); }
                if (request.ExtraPickUp > 0) { lines.Add(AddNewSalesItemLine(request.ExtraPickUp, fullLoadId)); }
                if (request.ExtraDelivery > 0) { lines.Add(AddNewSalesItemLine(request.ExtraDelivery, fullLoadId)); }
                if (request.WaitingTime > 0) { lines.Add(AddNewSalesItemLine(request.WaitingTime, fullLoadId)); }
                if (request.Overnight > 0) { lines.Add(AddNewSalesItemLine(request.Overnight, fullLoadId)); }
                if (request.Others > 0) { lines.Add(AddNewSalesItemLine(request.Others, fullLoadId)); }
                if (request.BrokerAdvance > 0) { lines.Add(AddNewSalesItemLine(request.BrokerAdvance, fullLoadId)); }
                if (request.BrokerAdvanceFee > 0) { lines.Add(AddNewSalesItemLine(request.BrokerAdvanceFee, fullLoadId)); }
                if (request.Discount > 0) { lines.Add(AddNewDiscountItemLine(request.Discount, fullLoadId)); }
            }

            if (request.LumperFees > 0)
            {
                var unloadingId = GetItemId(context, service, "Unloading");
                if (request.LumperFees > 0) { lines.Add(AddNewSalesItemLine(request.LumperFees, unloadingId)); }
            }

            if (request.FuelSurcharge > 0)
            {
                var fuelChargeId = GetItemId(context, service, "Fuel Surcharge");
                if (request.FuelSurcharge > 0) { lines.Add(AddNewSalesItemLine(request.FuelSurcharge, fuelChargeId)); }
            }

            if (request.RestakingFee > 0)
            {
                var restakingFeeId = GetItemId(context, service, "Restacking Fee");
                if (request.RestakingFee > 0) { lines.Add(AddNewSalesItemLine(request.RestakingFee, restakingFeeId)); }
            }
            if (request.PortEntry > 0)
            {
                var portFeeId = GetItemId(context, service, "Port Fee");
                if (request.PortEntry > 0) { lines.Add(AddNewSalesItemLine(request.PortEntry, portFeeId)); }
            }



            //if (request.CongestionFee > 0) { lines.Add(AddNewSalesItemLine(request.CongestionFee, fuelChargeId)); }
            //if (request.BondFee > 0) { lines.Add(AddNewSalesItemLine(request.BondFee, fuelChargeId)); }


            return lines;
        }

        protected IOAuthSession GetAuthContext()
        {
            var oAuthConsumerContext = new OAuthConsumerContext
            {
                ConsumerKey = ConsumerKey,
                ConsumerSecret = ConsumerSecret,
                SignatureMethod = SignatureMethod.HmacSha1
            };
            return new OAuthSession(oAuthConsumerContext,
                                        RequestTokenUrl,
                                        OauthUrl,
                                        AccessTokenUrl);
        }
    }

}