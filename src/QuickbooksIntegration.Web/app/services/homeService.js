﻿angular.module("Application.Services").factory("homeService", function ($httpq) {
    return {
        createInvoice: function (payload) {
            return $httpq.post("/createInvoice", payload);
        },
        createDriverSettlement: function (payload) {
            return $httpq.post("/createDriverSettlement", payload);
        },
        createInvoices: function (payload) {
            return $httpq.post("/createInvoices", payload);
        },
        saveDirectory: function (payload) {
            return $httpq.post("/saveDirectory", payload);
        },
        checkInvoice: function (email, invoiceNumber, id) {
            return $httpq.get("/checkInvoices/" + email + "/" + invoiceNumber + "/" + id);
        },
        pendingInvoices: function (email) {
            return $httpq.get("/pendingInvoices/" + email);
        },
        pendingDriverSettlement: function (email) {
            return $httpq.get("/pendingDriverSettlement/" + email);
        },
        checkFtpInvoice: function (email) {
            return $httpq.get("/checkFtpInvoices/" + email);
        },
        checkUserFolder: function (email) {
            return $httpq.get("/checkUserFolder/" + email);
        },
        syncedInvoices: function (email) {
            return $httpq.get("/syncedInvoices/" + email);
        },
        syncedDriverSettlement: function (email) {
            return $httpq.get("/syncedDriverSettlement/" + email);
        }
    };
}); 