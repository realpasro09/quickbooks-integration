﻿angular.module("Application.Controllers").controller("syncController",
    function ($scope, userService, $location, homeService, toastr, Upload, blockUI, $q, $route, $document) {
        var user = userService.GetUser();
       
        angular.element(document).ready(function () {

            var criticalTime = 180;
            var nonCriticalTime = 300;
            var initCriticalHour = 8;
            var endCriticalHour = 17;

            var setCountDownValue = function () {
                var currentHour = new Date().getHours();
                if (currentHour >= initCriticalHour && currentHour < endCriticalHour)
                    $scope.countdownvalue = criticalTime;
                else
                    $scope.countdownvalue = nonCriticalTime;
            };
            
            setCountDownValue();
        });
        
        $scope.tabSelected = 1;
        if (!user) {
            $location.path("/login");
        }
        $scope.$parent.title = "Sync";
        $scope.importInvoices = function () {
            $("#excelFile").click();
        };
        $scope.$watch("excelFileData", function (newValue) {

            if (newValue && newValue[0]) {
                if ($scope.excelFileData) {
                    var file = $scope.excelFileData[0];
                    blockUI.start("Importing Invoices");
                    Upload.upload({
                        url: "/insertInvoices",
                        method: "POST",
                        fields: { 'userEmail': user.email },
                        file: file
                    }).success(function () {
                        homeService.pendingInvoices(user.email).then(function (invoiceData) {
                            $scope.invoices = invoiceData;
                            toastr.success("Invoices imported successfully.");
                            blockUI.stop();
                        });
                    }).error(function () {
                        toastr.error("There was an error trying to import the invoices");
                        blockUI.stop();
                    });
                }

            }
        });
        var buildPayload = function (invoice) {
            return {
                UserEmail: user.email,
                BrokerName: invoice.brokerName,
                LoadNumber: invoice.loadNumber,
                BillingAddress: invoice.billingAddress,
                City: invoice.city,
                Country: invoice.country,
                DueDate: invoice.dueByDate,
                InvoiceDate: invoice.invoiceDate,
                BasicFreightRate: invoice.basicFreightRate,
                ExtraPickUp: invoice.extraPickUp,
                ExtraDelivery: invoice.extraDelivery,
                WaitingTime: invoice.waitingTime,
                LumperFees: invoice.lumperFees,
                Overnight: invoice.overnight,
                Others: invoice.others,
                FuelSurcharge: invoice.fuelSurcharge,
                CongestionFee: invoice.congestionFee,
                BondFee: invoice.bondFee,
                BrokerAdvance: invoice.brokerAdvance,
                BrokerAdvanceFee: invoice.brokerAdvanceFee,
                Discount: invoice.discount,
                TotalAmount: invoice.totalAmount,
                CustomerReferenceNumber: invoice.customerReferenceNumber,
                PickUpDate: invoice.pickUpDate,
                PickUpCity: invoice.pickUpCity,
                DeliveryCity: invoice.deliveryCity,
                SOSInvoiceNumber: invoice.sosInvoiceNumber,
                RestakingFee: invoice.restakingFee ? invoice.restakingFee : 0,
                PortEntry: invoice.portEntry ? invoice.portEntry : 0
            }
        }

        var buildDriverPayload = function (driverSettlement) {
            return {
                UserEmail: user.email,
                TruckOwner: driverSettlement.truckOwner,
                Driver: driverSettlement.driver,
                TruckNumber: driverSettlement.truckNumber,
                FileNumber: driverSettlement.fileNumber,
                OoPay: driverSettlement.ooPay,
                BasicDriverPay: driverSettlement.basicDriverPay,
                WaitingTime: driverSettlement.waitingTime,
                ExtraPickUp: driverSettlement.extraPickUp,
                ExtraDelivery: driverSettlement.extraDelivery,
                EmptyMileDebit: driverSettlement.emptyMileDebit,
                ExtraMiles: driverSettlement.extraMiles,
                Overnight: driverSettlement.overnight,
                Others: driverSettlement.others,
                FuelSurcharge: driverSettlement.fuelSurcharge,
                DriverCash: driverSettlement.driverCash,
                Fuel: driverSettlement.fuel,
                FuelFee: driverSettlement.fuelFee,
                RefFuel: driverSettlement.refFuel,
                FuelRebate: driverSettlement.fuelRebate,
                EfsCashAdvances: driverSettlement.efsCashAdvances,
                EfsFee: driverSettlement.efsFee,
                WesternUnion: driverSettlement.westernUnion,
                OwnChecks: driverSettlement.ownChecks,
                Transcheck: driverSettlement.transcheck,
                AdvanceFee: driverSettlement.advanceFee,
                Loan: driverSettlement.loan,
                LoanFee: driverSettlement.loanFee,
                NegativeFile: driverSettlement.negativeFile,
                QuickPayFee: driverSettlement.quickPayFee,
                QuickPayPercent: driverSettlement.quickPayPercent,
                TruckLease: driverSettlement.truckLease,
                LicensePlate: driverSettlement.licensePlate,
                RoadTax: driverSettlement.roadTax,
                TrailerLease: driverSettlement.trailerLease,
                TrailerRent: driverSettlement.trailerRent,
                LumperFees: driverSettlement.lumperFees,
                ReDiesel: driverSettlement.reDiesel,
                ReWash: driverSettlement.reWash,
                ReTolls: driverSettlement.reTolls,
                ReRepairs: driverSettlement.reRepairs,
                ReMaintenance: driverSettlement.reMaintenance,
                ReHotel: driverSettlement.reHotel,
                ReWTickets: driverSettlement.reWTickets,
                ReOil: driverSettlement.reOil,
                ReTires: driverSettlement.reTires,
                ReBonus: driverSettlement.reBonus,
                ReOwnRepairs: driverSettlement.reOwnRepairs,
                ReOwnMaintenance: driverSettlement.reOwnMaintenance,
                ReParking: driverSettlement.reParking,
                ReIdleAir: driverSettlement.reIdleAir,
                ReFees: driverSettlement.reFees,
                ReOthers: driverSettlement.reOthers,
                AccidentalInsurance: driverSettlement.accidentalInsurance,
                OoPackage: driverSettlement.ooPackage,
                ChildSupport: driverSettlement.childSupport,
                Insurance: driverSettlement.insurance,
                Yard: driverSettlement.yard,
                DotComplaince: driverSettlement.dotComplaince,
                Escrow: driverSettlement.escrow,
                CoDriverPay: driverSettlement.coDriverPay,
                OoAdvance: driverSettlement.ooAdvance,
                OoAdvanceCoDriver: driverSettlement.ooAdvanceCoDriver,
                OdOthers: driverSettlement.odOthers,
                Signs: driverSettlement.signs,
                Simplex: driverSettlement.simplex,
                FinePenalty: driverSettlement.finePenalty,
                CheckAmount: driverSettlement.checkAmount,
                Address: driverSettlement.address,
                City: driverSettlement.city,
                State: driverSettlement.state,
                Zip: driverSettlement.zip,
                Country: driverSettlement.country,
                DriverType: driverSettlement.driverType,
                SubTotal1: driverSettlement.subTotal1,
                SubTotal2: driverSettlement.subTotal2,
                SubTotal3: driverSettlement.subTotal3,
                SubTotal4: driverSettlement.subTotal4,
                SubTotal5: driverSettlement.subTotal5,
                SubTotal6: driverSettlement.subTotal6,
                TransactionType: driverSettlement.transactionType,
                Maintenance: driverSettlement.maintenance
            }
        }

        $scope.startTimer = function () {
            $scope.$broadcast("timer-start");
            $scope.ableToSearch = true;
        };

        $scope.resetClock = function () {
            $scope.$broadcast("timer-reset");
        }

        $scope.finished = function () {
            $scope.$broadcast("timer-reset");
            $scope.$broadcast("timer-resume");
            $scope.$apply();
            $scope.checkFtpAndSync();
        };

        $scope.i = 0;
        $scope.ii = 0;

        $scope.checkFtpAndSync = function () {
            var promiseArray = [];
            var externalPromiseArray = [];
            var promiseDriverArray = [];

            blockUI.start("Checking Ftp");
            homeService.checkFtpInvoice(user.email).then(function () {
                homeService.pendingInvoices(user.email).then(function (invoiceData) {
                    $scope.i = 1;
                    $scope.ii = 0;
                    invoiceData.forEach(function (invoice) {
                        var defered = $q.defer();
                        var innerDefer = $q.defer();
                        $q.all(promiseArray).then(function () {
                            var payload = buildPayload(invoice);
                            blockUI.start("syncing Invoice #" + invoice.sosInvoiceNumber + " " + $scope.i + " of " + invoiceData.length);
                            homeService.createInvoice(payload).then(function () {
                                $scope.i = $scope.i + 1;
                                $scope.ii = $scope.ii + 1;
                                homeService.syncedInvoices(user.email).then(function (syncedInvoice) {
                                    blockUI.stop();
                                    blockUI.stop();
                                    $scope.invoices = syncedInvoice;
                                    defered.resolve();
                                    innerDefer.resolve();
                                }, function () {
                                    blockUI.stop();
                                    toastr.error("There were some errors trying to sync the invoices, Please try again");
                                    defered.reject();
                                    innerDefer.reject();
                                });
                            }, function () {
                                blockUI.stop();
                                toastr.error("There were some errors trying to sync the invoices, Please try again");
                                defered.reject();
                                innerDefer.reject();
                            });
                        });
                        promiseArray.push(defered.promise);
                        externalPromiseArray.push(innerDefer.promise);
                    });

                    $q.all(externalPromiseArray).then(function () {
                        homeService.pendingDriverSettlement(user.email).then(function (driverData) {
                            $scope.d = 1;
                            $scope.dd = 0;
                            driverData.forEach(function (driver) {
                                var driverDefer = $q.defer();
                                $q.all(promiseDriverArray).then(function () {
                                    var payload = buildDriverPayload(driver);
                                    blockUI.start("syncing Driver Settlement #" + driver.fileNumber + " " + $scope.d + " of " + driverData.length);
                                    homeService.createDriverSettlement(payload).then(function () {
                                        $scope.d = $scope.d + 1;
                                        $scope.dd = $scope.dd + 1;
                                        homeService.syncedDriverSettlement(user.email).then(function (syncedDrivers) {
                                            blockUI.stop();
                                            blockUI.stop();
                                            $scope.driverSettlement = syncedDrivers;
                                            driverDefer.resolve();
                                        }, function () {
                                            blockUI.stop();
                                            toastr.error("There were some errors trying to sync the driver settlement, Please try again");
                                            driverDefer.reject();
                                        });
                                    }, function () {
                                        blockUI.stop();
                                        toastr.error("There were some errors trying to sync the driver settlement, Please try again");
                                        driverDefer.reject();
                                    });
                                });
                                promiseDriverArray.push(driverDefer.promise);
                            });
                            if (driverData.length === 0) {
                                blockUI.stop();
                            }
                        });
                    });
                });
            });
        }
        homeService.syncedDriverSettlement(user.email).then(function (syncedDrivers) {
            $scope.driverSettlement = syncedDrivers;
            $scope.startTimer();
        });
    });
