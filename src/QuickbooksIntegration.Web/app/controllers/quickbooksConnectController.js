﻿angular.module("Application.Controllers").controller("quickbooksConnectController",
    function ($scope, userService, $location, adminService) {
        var user = userService.GetUser();
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return "";
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    var oauthToken = getParameterByName("oauth_token");
    var oauthVerifier = getParameterByName("oauth_verifier");
    var realmId = getParameterByName("realmId");
    var dataSource = getParameterByName("dataSource");
    if (oauthToken) {
        var payload = {
            OauthToken: oauthToken,
            OauthVerifier: oauthVerifier,
            RealmId: realmId,
            DataSource: dataSource,
            UserEmail: user.email
        };

        adminService.saveQuickbookToken(payload).then(function () {
            var host = $location.host();
            if (host === "localhost") {
                host = location.host;
            }
            var newUrl = "http://" + host + "/#/home";
            window.opener.location.href = newUrl;
            window.close();
        });
    } else {
        var host = $location.host();
        //if (host === "localhost") {
        //    host = location.host;
        //}
        adminService.initQuickBooks(user.email, host).then(function (data) {
            console.log("authLink: ", data);
            window.location.assign(data);
        });
    }
});