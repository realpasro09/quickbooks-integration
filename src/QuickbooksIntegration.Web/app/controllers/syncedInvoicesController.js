﻿angular.module("Application.Controllers").controller("syncedInvoicesController",
    function ($scope, userService, $location, homeService, toastr, blockUI) {
        var user = userService.GetUser();
        if (!user) {
            $location.path("/login");
        }

        $scope.$parent.title = "Synced Invoices";
        blockUI.start("Getting Pending Invoices");
        homeService.syncedInvoices(user.email).then(function (invoiceData) {
            $scope.invoices = invoiceData;
            blockUI.stop();
        });
    });
