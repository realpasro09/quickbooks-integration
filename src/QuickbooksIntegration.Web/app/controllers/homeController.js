﻿angular.module("Application.Controllers").controller("homeController",
    function ($scope, userService, $location, homeService, toastr, Upload, blockUI, $q, $route) {
        var user = userService.GetUser();
        $scope.tabSelected = 1;
        if (!user) {
            $location.path("/login");
        }        
        $scope.saveDirectory = function () {
            var payload = {
                UserEmail: user.email,
                Directory: $scope.directory
            }
            homeService.saveDirectory(payload)
                .then(function () {
                    setTimeout(function() {
                        $route.reload();
                    }, 500);
                });
        }
        $scope.$parent.title = "Home";

        homeService.syncedInvoices(user.email).then(function (syncedInvoice) {
            $scope.invoices = syncedInvoice;
            homeService.syncedDriverSettlement(user.email).then(function (syncedDrivers) {
                $scope.driverSettlement = syncedDrivers;
                // $scope.startTimer();
            });
        });
    });
