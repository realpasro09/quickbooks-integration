﻿angular.module("Application.Controllers").controller("connectToQuickbooksController", function ($scope, userService, $location, adminService) {
    $.ajax({
        url: "https://appcenter.intuit.com/Content/IA/intuit.ipp.anywhere.js",
        type: "GET",
        dataType: "script",
        timeout: 4000,
        success: function () {
            if (typeof window.intuit !== "undefined") {
                var host = $location.host();
                if (host === "localhost") {
                    host = location.host;
                }
                var newUrl = "http://" + host + "/#/quickbooksConnect";
                window.intuit.ipp.anywhere.setup({
                    grantUrl: newUrl,
                    datasources: {
                        quickbooks: true,
                        payments: true
                    },
                    paymentOptions: {
                        intuitReferred: true
                    }
                });
            }
        },
        error: function (x, t, m) {
            alert("Intiut scripts are not working, please contact your system administrator");
            // show some friendly error message about Intuit downtime
        }
    });
});