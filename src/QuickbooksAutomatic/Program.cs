﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using RestClient = Rest.RestClient;

namespace QuickbooksAutomatic
{
    class Program
    {
        static void Main(string[] args)
        {
            //const string url = "http://quickbooksonlineintegration.apphb.com/checkFtpInvoice/global2014@quickbooks.com";
            const string checkFtpUrl = "http://localhost:8001/checkFtpInvoice/global2014@quickbooks.com";
            var result1 = GetRequest(checkFtpUrl);
            const string getPendingInvoicesUrl = "http://localhost:8001/pendingInvoices/global2014@quickbooks.com";
            var pendingInvoices = GetRequest(getPendingInvoicesUrl);
        }

        public chekFtp(string user)
        {
            var restClient = new RestClient("http://localhost:8001/");
            var request = new RestRequest(Method.GET)
            {
                Resource = "checkFtpInvoice/{user}"
            };
            request.AddParameter("user", user, ParameterType.UrlSegment);

            var response = restClient.GetAsync<string>(request);

            _userLogin = response.Data;

            return response.Data;
        }
    }
}
