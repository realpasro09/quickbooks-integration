using System;
using AcklenAvenue.Commands;

namespace QuickbooksIntegration.Domain
{
    public class VisitorSession : IUserSession
    {
        #region IUserSession Members

        public Guid Id { get; private set; }

        #endregion
    }
}