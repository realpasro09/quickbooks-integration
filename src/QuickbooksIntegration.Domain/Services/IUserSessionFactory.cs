using QuickbooksIntegration.Domain.Entities;

namespace QuickbooksIntegration.Domain.Services
{
    public interface IUserSessionFactory
    {
        UserLoginSession Create(User executor);
    }
}