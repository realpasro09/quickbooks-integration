using QuickbooksIntegration.Domain.ValueObjects;

namespace QuickbooksIntegration.Domain.Services
{
    public interface IPasswordEncryptor
    {
        EncryptedPassword Encrypt(string clearTextPassword);
    }
}