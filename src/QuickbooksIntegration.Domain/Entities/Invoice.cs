﻿using System;

namespace QuickbooksIntegration.Domain.Entities
{
    public class Invoice : Entity
    {
        public Invoice()
        {
            Id = Guid.NewGuid();
        }
        public virtual User User { get; set; }
        public virtual string QbId { get; set; }
        public virtual string DatabaseName { get; set; }
        public virtual string BrokerName { get; set; }
        public virtual string BillingAddress { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string ZipCode { get; set; }
        public virtual string Country { get; set; }
        public virtual string SosInvoiceNumber { get; set; }
        public virtual string LoadNumber { get; set; }
        public virtual string InvoiceDate { get; set; }
        public virtual string DueByDate { get; set; }
        public virtual string CustomerReferenceNumber { get; set; }
        public virtual string PickUpDate { get; set; }
        public virtual string InternalReferenceNumber { get; set; }
        public virtual string PickUpCity { get; set; }
        public virtual string DeliveryDate { get; set; }
        public virtual string DeliveryCity { get; set; }
        public virtual string PCs { get; set; }
        public virtual string Weight { get; set; }
        public virtual string Miles { get; set; }
        public virtual string Notes { get; set; }
        public virtual string BasicFreightRate { get; set; }
        public virtual string ExtraPickUp { get; set; }
        public virtual string ExtraDelivery { get; set; }
        public virtual string WaitingTime { get; set; }
        public virtual string LumperFees { get; set; }
        public virtual string Overnight { get; set; }
        public virtual string Others { get; set; }
        public virtual string FuelSurcharge { get; set; }
        public virtual string CongestionFee { get; set; }
        public virtual string BondFee { get; set; }
        public virtual string BrokerAdvance { get; set; }
        public virtual string BrokerAdvanceFee { get; set; }
        public virtual string Discount { get; set; }
        public virtual string TotalAmount { get; set; }
        public virtual string FootNotes { get; set; }
        public virtual string DispatcherName { get; set; }
        public virtual string Overweight { get; set; }
        public virtual string PerDiem { get; set; }
        public virtual string BorderFee { get; set; }
        public virtual string Hazardous { get; set; }
        public virtual string QuickPayFee { get; set; }
        public virtual string Class { get; set; }
        public virtual bool MarkAsUpdate { get; set; }
        public virtual string SyncDate { get; set; }
        public virtual string Status { get; set; }

        public virtual string RestakingFee { get; set; }
        public virtual string PortEntry { get; set; }
        public virtual bool InSyncProcess { get; set; }
    }
}