using System;

namespace QuickbooksIntegration.Domain
{
    public interface IEntity
    {
        Guid Id { get; }
    }
}