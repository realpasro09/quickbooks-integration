﻿namespace QuickbooksIntegration.Domain
{
    public enum ValidationFailureType
    {
        Missing,
        DoesNotExist,
        Expired
    }
}